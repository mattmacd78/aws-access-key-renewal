aws-access-key-renewal
======================

A script to ease, enforce & automate AWS ACCESS KEY expiry & rotation.

#### Description

Default behaviour is as follows :

- parse the `AWS_CRED_STORE` INI file
- iterate over each `AWS_PROFILE`
	- query the AWS API for IAM AccessKeys *for the current user*
	- test the expiry of each AccessKey
		- if the AccessKey has expired :
			- create a `NewAccessKey`
			- re-configure the current AWS CLI profile
			- delete the `OldAccessKey`
		- else do nothing

#### Requirements

- `aws-cli`
- `jq`

Using the AWS Named Profiles with the `aws-cli` :

- https://docs.aws.amazon.com/cli/latest/userguide/cli-multiple-profiles.html

#### Compatibility

Tested versus :

- Linux
- MacOS
- GNU `date`
- BSD `date`

#### Basic Usage

		# ./aws-access-key-renewal.sh
		AWS_PROFILE=***********
		AWS_ACCESS_KEY_ID=****************6O3A
		AccessKey="{"UserName":"*********","AccessKeyId":"****************6O3A","Status":"Active","CreateDate":"2017-12-16T09:08:23Z"}"
		This is the currently used key
		key created on 2017-12-16T09:08:23Z : key has expired and should be renewed (103 days old)
		NewAccessKeyId="****************789F"
		AWS Access Key ID [****************6O3A]: AWS Secret Access Key [****************cmBG]: Default region name [eu-west-1]: Default output format [json]:
		Deleting OldAccessKeyId="****************6O3A".....DONE!

#### Available Variables

| Variable         | Default                  |
|------------------|--------------------------|
| `MAX_DAYS`       | 90                       |
| `AWS_CRED_STORE` | "$HOME/.aws/credentials" |


#### Overriding Defaults

- Enforcing a single profile

		export AWS_PROFILE="my-aws-profile"

This prevents the script from looping over each profile defined in `AWS_CRED_STORE`

- Enforcing a different expiry

		export MAX_DAYS=0

- Choosing a different `AWS_CRED_STORE`

		export AWS_CRED_STORE="/tmp/my-custom-aws-creds.ini"
