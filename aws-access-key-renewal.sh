#!/bin/bash

# Enable logging

# exec >> $(basename $0 .sh).$(date "+%Y-%m-%d_%Hh%Mm%Ss").log
# exec 2>&1

# Anonymize output

anonymize()
{
  this_var="${*}"
  i=0
  while [ $i -le ${#this_var} ]
  do
    if [ `echo "( $i % 2 )" | bc` == 0 ]
    then
      # POS is EVEN
      printf "${this_var:$i:1}"
    else
      # POS is ODD
      printf "*"
    fi
    let i+=1
  done
  echo
}

# Dependency check

deps="aws jq"
for dep in ${deps} ; do if ! which "${dep}" &>/dev/null ; then echo "${dep} is missing" ; exit 1 ; fi ; done

# Set defaults if not overriden by user

if [ -z $MAX_DAYS       ] ; then MAX_DAYS=90                             ; fi
if [ -z $AWS_CRED_STORE ] ; then AWS_CRED_STORE="$HOME/.aws/credentials" ; fi

# Display config

echo "AWS_PROFILE=\"${AWS_PROFILE}\""
echo "MAX_DAYS=\"${MAX_DAYS}\""
echo "AWS_CRED_STORE=\"${AWS_CRED_STORE}\""
echo

# AWS related functions

aws_list_keys()         { aws --output json iam list-access-keys  --profile "${*}" | jq -cr ".AccessKeyMetadata[]" ; }

aws_create_key()        { aws --output json iam create-access-key --profile "${*}" | jq -cr ".AccessKey" ; }
aws_delete_key()        { aws --output json iam delete-access-key --access-key-id "${1}" --profile "${2}" ; }

aws_access_key_id()     { aws configure get aws_access_key_id     --profile "${*}" ; }
aws_secret_access_key() { aws configure get aws_secret_access_key --profile "${*}" ; }

aws_configure()         { printf "${1}\n${2}\n\n\n\n" | aws configure --profile "${3}" ; echo ; }

# JQ related function

get_attr() { echo "${2}" | jq -cr ".${1}" ; }

# DATE related functions

maxage()        { echo "( 60 * 60 * 24 * ${MAX_DAYS} )" | bc ; }
epoch_to_days() { echo "( ${*} / 60 / 60 / 24 )" | bc ; }

# BSD date (MacOS)

if man date | grep BSD &>/dev/null
then
    export LANG=en_US.UTF-8
    now()      { date -j -f "%a %b %d %T %Z %Y"  "$(date)" "+%s" ; }
    to_epoch() { date -j -f "%Y-%m-%dT%H:%M:%SZ" "${*}"    "+%s" ; }
fi

# GNU date (Linux)

if man date | grep GNU &>/dev/null
then
    now()      { date "+%s" ; }
    to_epoch() { date -d "${*}" "+%s" ; }
fi

# Key-expiry testing function

has_expired()
{
    CreateDate=$(get_attr CreateDate "${*}")
    NOW=$(now)
    KEYDATE=$(to_epoch "${CreateDate}")
    MAXAGE=$(maxage)
    KEYAGE=$(epoch_to_days "$(( $NOW - $KEYDATE ))")
    DAYS=$(epoch_to_days "$(( $NOW - $KEYDATE - $MAXAGE ))" | tr -d '-')

    printf " > key created on ${CreateDate} "
    if [ $(( $NOW - $KEYDATE )) -ge $MAXAGE ]
    then
        echo ": key has expired and should be renewed ($KEYAGE days old)"
        true
    else
        echo ": key created $KEYAGE days ago (expires in $DAYS days)"
        false
    fi
}

## MAIN

# Limit upcoming processing to the current AWS_PROFILE when already set

if [ ! -z $AWS_PROFILE ]
then
    AWS_PROFILES="${AWS_PROFILE}"
else
    AWS_PROFILES=$(grep ^\\[ "${AWS_CRED_STORE}" | tr -d '[' | tr -d ']')
fi

# Loop over each AWS_PROFILE

for AWS_PROFILE in $AWS_PROFILES
do
    printf "> AWS_PROFILE=\"${AWS_PROFILE}\" "
    AWS_ACCESS_KEY_ID=$(aws_access_key_id $AWS_PROFILE)

    echo "AWS_ACCESS_KEY_ID=\"`anonymize ${AWS_ACCESS_KEY_ID}`\""
    AccessKeys=$(aws_list_keys "${AWS_PROFILE}")

    # Loop over each AccessKey returned by API

    for AccessKey in ${AccessKeys}
    do
        # echo "AccessKey=\"${AccessKey}\""

        # Test wether or not AccessKey is the one used by current AWS_PROFILE

        AccessKeyId=$(get_attr AccessKeyId "${AccessKey}" )
        if [ ${AccessKeyId} == ${AWS_ACCESS_KEY_ID} ]
        then
            echo " > This is the currently used key"
        fi

        # Test AccessKey expiry

        if has_expired "${AccessKey}"
        then
            OldKey="${AccessKey}"

            # Create NewKey

            printf "  > Creating NewKey : "
            NewKey=$(aws_create_key "${AWS_PROFILE}")
            NewAccessKeyId=$(get_attr AccessKeyId "${NewKey}" )
            NewSecretAccessKey=$(get_attr SecretAccessKey "${NewKey}" )
            echo "NewAccessKeyId=\"`anonymize ${NewAccessKeyId}`\""

            # Re-configure AWS_PROFILE to use NewKey

            printf "  > Re-Configuring AWS_PROFILE=\"${AWS_PROFILE}\" to use NewAccessKeyId=\"`anonymize ${NewAccessKeyId}`\" : "
            aws_configure "${NewAccessKeyId}" "${NewSecretAccessKey}" "${AWS_PROFILE}"

            # Delete OldKey

            OldAccessKeyId=$(get_attr AccessKeyId "${OldKey}" )
            printf "  > Deleting OldAccessKeyId=\"`anonymize ${OldAccessKeyId}`\""
            RETCODE=""

            # Loop until key deletion succeeds

            until [ "${RETCODE}" == "0" ]
            do
                aws_delete_key "${OldAccessKeyId}" "${AWS_PROFILE}" 2>/dev/null
                RETCODE="$?"
                printf "."
                sleep 1
            done && echo " DONE!"
        fi
        echo
      done
      echo
done
